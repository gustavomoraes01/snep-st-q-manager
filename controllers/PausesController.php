<?php

/**
 * Q-Manager Pauses Controller
 *
 * @category  Snep
 * @package   Q-Manager
 * @copyright Copyright (c) 2016 OpenS Tecnologia
 * @author    Tiago Zimmermann <tiago.zimmermann@opens.com.br>
 */
class SnepQManager_PausesController extends Zend_Controller_Action {

    /**
     * Initial settings of the class
     */
     public function init() {
        $this->view->url = $this->getFrontController()->getBaseUrl() . '/' . $this->getRequest()->getModuleName(). '/' . $this->getRequest()->getControllerName();
        $this->view->lineNumber = Zend_Registry::get('config')->ambiente->linelimit;

        $this->connector = "http://127.0.0.1:3000/config/pauses";

        $this->view->baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
        $this->view->key = Snep_Dashboard_Manager::getKey(
            Zend_Controller_Front::getInstance()->getRequest()->getModuleName(),
            Zend_Controller_Front::getInstance()->getRequest()->getControllerName(),
            Zend_Controller_Front::getInstance()->getRequest()->getActionName());
    }

    /**
     * List all Cost Center's
     */
    public function indexAction() {

        $this->view->breadcrumb = Snep_Breadcrumb::renderPath(array(
                    $this->view->translate("Q-Manager"),
                    $this->view->translate("Pauses")));

        $pauses = Pauses_Manager::getAll($this->connector);

        if(is_int($pauses) && $pauses != 204){
            $message = $this->view->translate("Error: Code ") . $pauses . $this->view->translate(". Please contact the administrator.");
            $this->_helper->redirector('sneperror','error','default',array('error_message'=>$message));
        }else{
            $this->view->pauses = $pauses;
        }

    }

    /**
     * Add pause
     */
    public function addAction(){

        $this->view->breadcrumb = Snep_Breadcrumb::renderPath(array(
                    $this->view->translate("Q-Manager"),
                    $this->view->translate("Pauses"),
                    $this->view->translate("Add")));

        $pauses = Pauses_Manager::getAll($this->connector);
        if($pauses == 204){
            $this->view->code = 5;
        }else{
            $count = count($pauses);
            $this->view->code = $pauses[$count-1]->code + 1;
        }

        $this->renderScript( $this->getRequest()->getControllerName().'/add.phtml' );

        if($this->_request->getPost()) {

            $dados = $this->_request->getParams();
            $url = $this->connector."/?agent=".$dados['code'];
            Pauses_Manager::add($dados,$this->connector);

            if($httpcode = 200){
                $this->_redirect($this->getRequest()->getModuleName().'/'.$this->getRequest()->getControllerName());
            }else{
                $message = $this->view->translate("Error: Code ") . $httpcode . $this->view->translate(". Please contact the administrator.");
                $this->_helper->redirector('sneperror','error','default',array('error_message'=>$message));
            }

        }

    }

    /**
     * Edit pause
     */
    public function editAction(){

        $this->view->breadcrumb = Snep_Breadcrumb::renderPath(array(
                    $this->view->translate("Q-Manager"),
                    $this->view->translate("Pauses"),
                    $this->view->translate("Edit")));

        $id = $this->_request->getParam('id');
        $url = $this->connector."/?code=".$id;
        $pause = Pauses_Manager::getPauseByCode($url);

        //Define the action and load form
        $this->view->pause = $pause;
        $this->renderScript( $this->getRequest()->getControllerName().'/edit.phtml' );

        // After POST
        if ($this->_request->getPost()) {

            $dados = $this->_request->getParams();
            $httpcode = Pauses_Manager::edit($dados, $this->connector);
            $this->_redirect($this->getRequest()->getModuleName().'/'.$this->getRequest()->getControllerName());

        }

    }

    /**
     * Remove pause
     */
    public function removeAction(){

        $this->view->breadcrumb = Snep_Breadcrumb::renderPath(array(
                    $this->view->translate("Q-Manager"),
                    $this->view->translate("Pausas"),
                    $this->view->translate("Remove")));

        $id = $this->_request->getParam('id');

        $this->view->id = $id;
        $this->view->remove_title = $this->view->translate('Remove Pause.');
        $this->view->remove_message = $this->view->translate('The pause will be deleted. After that, you have no way to recover it.');
        $this->renderScript( $this->getRequest()->getControllerName().'/remove.phtml' );

        if ($this->_request->getPost()) {

            Pauses_Manager::remove($_POST['id'], $this->connector);
            $this->_redirect($this->getRequest()->getModuleName().'/'.$this->getRequest()->getControllerName());
        }
    }

}