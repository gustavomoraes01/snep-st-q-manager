<?php

/**
 * Q-Manager Access
 *
 * @category  Snep
 * @package   Q-Manager
 * @copyright Copyright (c) 2017 OpenS Tecnologia
 * @author    Tiago Zimmermann <tiago.zimmermann@opens.com.br>
 */
class SnepQManager_QmanagerController extends Zend_Controller_Action {

    public function preDispatch() {
        $this->_helper->layout()->setLayoutPath(realpath(dirname(__FILE__) . "/../views/layouts"));
        $this->_helper->layout()->setLayout("empty");
    }

    /**
    * Redirect page qmanager interface
    */
    public function indexAction() {

        $qmanager = "http://".$_SERVER["HTTP_HOST"].':8080/';
        $this->_redirect($qmanager);
    }
}

?>
